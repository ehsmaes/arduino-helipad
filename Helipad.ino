/*
 * Helipad Arduino sketch.
 *
 * Free to use but if you do, please reference the Instructable. And why not leave a comment in the instructable?
 *
 * Instructables user: ehsmaes
 */

#include <LiquidCrystal.h>

boolean debug=false; // set to true for debug printouts to serial

// Ultrasound sensor
byte ultraSoundInputPin=8; //ECHO pin
byte ultraSoundOutputPin=7; //TRIG pin
unsigned int approachDistance=80;
unsigned int landDistance=5;

// LCD Display
LiquidCrystal lcd(12, 13, 5, 4, 3, 2);
byte lcdBackLightPin=11; // PWM capable pin number
byte lcdRows=4;
const int lcdCols=20;
char lcdLine[lcdCols+1];
byte lcdHigh=127;
byte lcdLow=25;

// Global variables and timers
const unsigned long maxlong = 4294967295;
unsigned long flutter_approach = 3000;
unsigned long t_approach = 0;
unsigned long t_approach_ref = 0;
unsigned int t_approach_seconds = 0;
unsigned int t_approach_delay = 4;
unsigned long flutter_land = 1500;
unsigned long t_land = 0;
unsigned long t_lap = 0;
unsigned long t_lap_ref = 0;

// RGB LED Amp -- see http://www.instructables.com/id/LED-Strip-Controller-w-LED-Amp-Arduino/
typedef struct {
	int r;
	int g;
	int b;
	int rPin; // PWM capable pin number
	int gPin; // PWM capable pin number
	int bPin; // PWM capable pin number
} ledtype;
ledtype led={0, 0, 0, 9, 6, 10};
byte ledHigh=235; // using LED Amp the PWM duty cycle is reversed. Lower numbers give higher illuminance.
byte ledLow=255;  // led off

// State machine enum
enum padStateEnum {
	idle,
	approaching,
	landing,
	landedOk,
	landedNok
};

padStateEnum padState=idle;
padStateEnum padStatePrevious=idle;

void setup()
{
	// Ultrasound setup
	pinMode(ultraSoundInputPin, INPUT);
	pinMode(ultraSoundOutputPin, OUTPUT);
	// LCD setup
	lcd.begin(lcdCols, lcdRows);
	lcd.print("   -= HELIPAD =-");
	pinMode(lcdBackLightPin, OUTPUT);
	analogWrite(lcdBackLightPin, lcdLow);
	// RGB LED
	pinMode(led.rPin, OUTPUT);
	pinMode(led.gPin, OUTPUT);
	pinMode(led.bPin, OUTPUT);
	analogWrite(led.rPin, ledLow);
	analogWrite(led.gPin, ledLow);
	analogWrite(led.bPin, ledLow);
	// Serial
	Serial.begin(9600);
}

void loop()
{
	String status = "                    ";
	unsigned int distance = read_distance();
	
	if(debug && (distance < 100)) {
		Serial.print("distance: ");
		Serial.println(distance);
	}
	
	// Evaluate transition conditions
	boolean idle_to_approaching =        padStatePrevious == idle        && in_zone_approach(distance); // Transition (1)
	boolean approaching_to_idle =        padStatePrevious == approaching && !in_zone_approach(distance) && !in_zone_land(distance); // Transition (2)
	boolean approaching_to_landing =     padStatePrevious == approaching && in_zone_approach(distance)  && (t_approach_seconds > t_approach_delay); // Transition (3)
	boolean approaching_to_landedNok =   padStatePrevious == approaching && in_zone_land(distance)      && (t_approach_seconds <= t_approach_delay); // Transition (4)
	boolean landing_to_landedOk =        padStatePrevious == landing     && in_zone_land(distance);     // Transition (5)
	boolean idle_to_landedNok =          padStatePrevious == idle        && in_zone_land(distance);     // Transition (6)
	boolean landedNok_to_idle =          padStatePrevious == landedNok   && !in_zone_approach(distance) && !in_zone_land(distance); // Transition (7)
	boolean landing_to_idle =            padStatePrevious == landing     && !in_zone_approach(distance) && !in_zone_land(distance); // Transition (8)
	boolean landedOk_to_idle =           padStatePrevious == landedOk    && !in_zone_approach(distance) && !in_zone_land(distance); // Transition (9)
	boolean approaching_to_approaching = padStatePrevious == approaching && in_zone_approach(distance)  && (t_approach_seconds <= t_approach_delay); // Transition (10)
	
	// Transition to state
	if (idle_to_approaching) {
		status = "Aircraft Approaching";
		t_approach_ref = millis(); // set timer for approach
		padState = approaching;
		analogWrite(lcdBackLightPin, lcdHigh);
	} else if (approaching_to_approaching) {
		t_approach_seconds = (millis() - t_approach_ref) / 1000; // seconds since state change
		padState = approaching;
		analogWrite(lcdBackLightPin, lcdHigh);
	} else if (approaching_to_idle || landedNok_to_idle	|| landing_to_idle)	{
		status = "                    ";
		padState = idle;
		t_approach_ref = maxlong;
		t_approach_seconds = 0;
		analogWrite(lcdBackLightPin, lcdLow);
	} else if (landedOk_to_idle)	{
		status = "                    ";
		padState = idle;
		t_approach_ref = maxlong;
		t_approach_seconds = 0;
		t_lap_ref = millis();
		analogWrite(lcdBackLightPin, lcdLow);
	} else if (approaching_to_landing) 	{
		status = " Permission To Land ";
		padState = landing;
		analogWrite(lcdBackLightPin, lcdHigh);
	} else if (approaching_to_landedNok || idle_to_landedNok) {
		status = "   Failed Landing   ";
		padState = landedNok;
		analogWrite(lcdBackLightPin, lcdHigh);
	} else if (landing_to_landedOk) {
		status = "   Good Landing    ";
		padState = landedOk;
		analogWrite(lcdBackLightPin, lcdHigh);
		Serial.print("Lap: ");
		Serial.println((float)t_lap * 1.0 / 1000.0);
	}
	
	char str_label[20] = "   Lap:    ";
	if (padState != landedOk) {
		t_lap = millis() - t_lap_ref;
		sprintf(str_label, "   Flight: ");
	}

	// Print timer to LCD	
	lcd.setCursor(0,1);
	lcd.print(str_label);
	lcd.print((float)t_lap / 1000.0,2);
	lcd.print("  ");
	
	if (padStatePrevious != padState) {
		lcd.setCursor(0,3);
		lcd.print(status);
				
		// debug
		if(debug) {
			Serial.println("--debug--");
			Serial.print("	distance: ");
			Serial.println(distance);
			if(idle_to_approaching)    {Serial.println("	transition: idle_to_approaching");} // Transition (1)
			if(approaching_to_idle)    {Serial.println("	transition: approaching_to_idle");} // Transition (2)
			if(approaching_to_landing) {Serial.println("	transition: approaching_to_landing");} // Transition (3)
			if(approaching_to_landedNok) {Serial.println("	transition: approaching_to_landedNok");} // Transition (4)
			if(landing_to_landedOk) {Serial.println("	transition: landing_to_landedOk");} // Transition (5)
			if(idle_to_landedNok) {Serial.println("	transition: idle_to_landedNok");}  // Transition (6)
			if(landedNok_to_idle) {Serial.println("	transition: landedNok_to_idle");}  // Transition (7)
			if(landing_to_idle) {Serial.println("	transition: landing_to_idle");}  // Transition (8)
			if(landedOk_to_idle) {Serial.println("	transition: landedOk_to_idle");} // Transition (9)
			if(approaching_to_approaching) {Serial.println("	transition: approaching_to_approaching");} // Transition (10)
			if(in_zone_approach(distance)) {Serial.println("	in_zone_approach");}
			if(in_zone_land(distance)) {Serial.println("	in_zone_land");}
			Serial.println("---------");
		}
	}
	
	updateLed();
	padStatePrevious = padState;
}


//--------------- Functions ---------------//

long msToCentimeters(long ms) {return (ms / 29) / 2;}

unsigned int read_distance() {
	
	digitalWrite(ultraSoundOutputPin, HIGH); //Trigger ultrasonic detection
	delayMicroseconds(10);
	digitalWrite(ultraSoundOutputPin, LOW);
	unsigned int echo = pulseIn(ultraSoundInputPin, HIGH); //Read ultrasonic reflection
	
	return msToCentimeters(echo);
}

// return true if object is in approach distance (or has been recently)
boolean in_zone_approach(unsigned int distance) {
	if(distance < approachDistance && distance > landDistance)	 {
		t_approach = millis() + flutter_approach;
	}
	return ((millis() < t_approach) && (distance > landDistance));
}

boolean in_zone_land(unsigned int distance) {
	if(distance <= landDistance)	 {
		t_land = millis() + flutter_land;
	}
	return (millis() < t_land);
}

void setLed() {
	analogWrite(led.rPin, led.r);
	analogWrite(led.gPin, led.g);
	analogWrite(led.bPin, led.b);
}

void setLedWhite() {
	led.r=ledHigh;
	led.g=ledHigh;
	led.b=ledHigh;
	setLed();
}

void setLedRed() {
	led.r=ledHigh;
	led.g=ledLow;
	led.b=ledLow;
	setLed();
}

void setLedGreen() {
	led.r=ledLow;
	led.g=ledHigh;
	led.b=ledLow;
	setLed();
}

void setLedBlue() {
	led.r=ledLow;
	led.g=ledLow;
	led.b=ledHigh;
	setLed();
}

void setLedOff() {
	led.r=ledLow;
	led.g=ledLow;
	led.b=ledLow;
	setLed();
}

void updateLed()  {
	switch (padState) {
		case idle:
			if((millis() % 1000) < 50) {
				setLedWhite();
			} else {
				setLedOff();
			}
			break;
		case approaching:
			setLedWhite();
			break;
		case landedNok:
			setLedRed();
			break;
		case landedOk:
			setLedGreen();
			break;
		case landing:
			setLedBlue();
			break;
	}
	
}